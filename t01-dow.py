dow = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
#Tasks
#1.	Print the first day of the week.
print("First day is %s" % dow[0])
#2.	Print the second day of the week.
print("Second day is {}".format( dow[1]) )
print(f"Third day is {dow[2]}")
#3.	Print the number of days.
print(f"Number of days:{len(dow)}")
#4.	Print the days of the week.
print("List of days: %s" % ','.join(dow))
#5.	Print the number of letters in the first day of the week.
print("Number of letters in first day: %s" % len(dow[0]))
#6.	Print the days that start with “S”
print("Days that start with S %s" % [s for s in dow if s[0]=='S'])
#7.	Print the last day of the week.
print("Last day of the week: %s" % dow[-1])
#8.	Print the days of the week in reverse order.
print("Days in reverse order: %s" % list(reversed(dow)))
#9.	Print the days of the week in option tags with the first day marked selected.
print("Days decorated with <option>: %s" %
        "".join([f"<option {'selected' if i==0 else ''}>{dow[i]}</option>"
            for i in range(0,len(dow))]))



