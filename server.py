from flask import Flask, render_template
import json, random

w = json.load(open('worldl.json'))

app = Flask(__name__)

@app.route('/')
def index():
    return f"<a href='quiz'>QUIZ</a>"

@app.route('/randquestion')
def randquestion():
    correct = random.randint(0, len(w)-1)
    d = {correct}
    while len(d) < 4:
        d.add(random.randint(0,len(w)-1))
    dl = list(d)
    dl.sort()
    rq = {"stem":w[correct]['name'],
            "distractors":[w[i]['flag'] for i in dl],
            "correctAnswer":w[correct]['flag']
    }
    return json.dumps(rq)

@app.route('/quiz')
def quiz():
    return render_template('quiz.html')

if __name__=='__main__':
    app.run(host='0.0.0.0',port = 5036, debug = True)
