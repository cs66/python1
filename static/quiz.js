var iam = 'U' + Math.floor(1000000 * Math.random());
var mqtt = new Paho.Client('tw.igs.farm',8083,'cid'+Math.random());
var topic = 'awh-quiz';
$(function(){
  mqtt.connect({
    useSSL: true,
    onSuccess:function(){
      console.log('connection success');
      mqtt.subscribe(topic);
    },
    onFailure:function(d){
      console.log('connection failed');
    }
  });
  mqtt.onMessageArrived = function(d){
    let m = JSON.parse(d.payloadString);
    showScores(m.scores);
    renderQuestion(m.question);
  }
})

$(function(){
  fetch('/randquestion')
    .then(x => x.json())
    .then(q => renderQuestion(q));
})

function renderQuestion(q){
  $('#stem').text(q.stem);
  $('#distractors').empty();
  for(let d of q.distractors){
    $('#distractors').append(
      $('<img/>', {src: 'https://tw.igs.farm/imgs/'+d,
        data:{val:d},
        click:function(){
          if (q.correctAnswer === $(this).data('val')){
            $('#congrat')
              .fadeIn(1000,function(){
                $(this).fadeOut(1000,function(){
                  let s = $('#score_table').data('all') || {};
                  s[iam] = 1+(s[iam] || 0);
                  fetch('/randquestion')
                    .then(x => x.json())
                    .then(q => mqtt.publish(topic,JSON.stringify(
                      {question:q,scores:s}
                    )))
                  });
              })
          }else{
            $('#commiss')
              .fadeIn(1000,function(){
                $(this).fadeOut(1000);
              })
          }
        }})
    );
  }
}

function showScores(s){
  if (s === undefined)
    return;
  $('#score_table')
    .data('all',s)
    .empty();
  for(let k in s){
    $('<tr/>')
       .append($('<td/>',{text:k}))
       .append($('<td/>',{text:s[k]}))
       .appendTo('#score_table');
  }
}






